package com.epam.jewels.type;

public enum JewelType {
    Opal,
    Ruby,
    Sapphire
}
