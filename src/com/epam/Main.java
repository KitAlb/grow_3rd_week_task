package com.epam;

import com.epam.jewels.Jewel;
import com.epam.necklaces.Necklace;

import java.util.List;

public class Main {

    // Камені. Визначити ієрархію дорогоцінних і напівкоштовних каменів.
    // Відібрати камені для намиста. Підрахувати загальну вагу (в каратах) і вартість.
    // Провести сортування каменів намиста на основі цінності.
    // Знайти камені в намисті, що відповідають заданому діапазону параметрів прозорості.

    public static void main(String[] args) {
        Necklace myNecklace1 = new Necklace();

        myNecklace1.addAll(JewelsGenerator.getRandomJewels());
        JewelsGenerator.getRandomJewel().ifPresent(myNecklace1::add);

        Necklace myNecklace2 = new Necklace(JewelsGenerator.getRandomJewels());

        printNecklaceProperties(myNecklace1, 0, 70);
        printNecklaceProperties(myNecklace2, 75, 100);
    }

    private static void printNecklaceProperties(Necklace necklace, int min, int max) {
        System.out.println();
        System.out.println("-----------------------------------------------------");
        System.out.println("Necklace's total weight: " + necklace.getTotalWeight() + " kar");
        System.out.println("Necklace's total price: " + necklace.getTotalPrice() + " USD");
        System.out.println();
        System.out.println("Jewels by price:");

        for (Jewel jewel : necklace.getSortedByPrice()) {
            System.out.println(jewel);
        }
        System.out.println();

        List<Jewel> jewels = necklace.serchByTransparency(min, max);
        if (jewels.size() > 0) {
            System.out.println("Jewels with transparency from " + min + " to " + max + ":");

            for (Jewel jewel : jewels) {
                System.out.println(jewel.getClass().getSimpleName() + ", Transparency = " + jewel.getTransparency() + "%");
            }
        } else {
            System.out.println("Necklace doesn't have jewels with transparency from " + min + " to " + max);
        }

        System.out.println("-----------------------------------------------------");
    }
}
